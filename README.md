# pyCaptcha

Un module de test type Captcha (test de Turing) pour vérifier que l'utilisateur n'est pas un robot. Ecrit en Python 3.

# Fonctionnement

Un mot est choisi aléatoirement dans une liste, l'utilisateur doit donner la valeur d'une lettre de ce dernier. Exemple : Quelle est la 3ème lettre du mot basketball ?

Si la réponse est exacte, la fonction renvoie la valeur verif_captcha à **True**. En revanche après 5 tentatives on considère que le test a échoué, la fonction renvoie la valeur verif_captcha à **False**.

# Licence

Ce script est sous [licence MIT](https://gitlab.com/groussel/pycaptcha/blob/master/LICENSE), c'est à dire que vous pouvez l'utiliser, le copier, le modifier, le fusionner, le publier, le distribuer, le vendre et changer sa licence. La seule obligation est de mettre le nom de(s) auteur(s) avec la notice de copyright.