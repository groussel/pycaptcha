#!/usr/bin/env python
import random

def pycaptcha():
    verif_captcha = False
    tent = 0
#   Déclaration de la liste de mots utilisés, sans accent et de préférence > 5 lettres 
    mots = ["bretagne", "soleil", "bouteille", "voiture", "animal", "musique", "table", "ordinateur", "programmation", "pantalon", "chaussure", "piscine", "cheveux", "chaussette", "clavier", "costume", "docteur", "barbichette", "lunette", "radio", "fauteuil", "arbre", "capitale", "monde", "capuche", "parquet", "garage", "escalier", "boisson", "rigoler", "cuisine", "chambre", "matelas", "tapis", "bureau", "radiateur", "espagne", "allemagne", "bruxelles", "luxembourg", "amsterdam", "norvege", "croatie", "basketball", "football", "handball", "australie", "autriche", "japon", "infirmier"]
#   print(len(mots)) # Affiche la longueur de la liste de mots utilisée
    while verif_captcha == False:
        x = random.randint(0,len(mots)-1) # On génère un nombre pour choisir un mot aléatoire dans la liste
        captcha = list(mots[x]) # On crée une nouvelle liste où on décompose les lettres du mot sélectionné au hasard
        y = random.randint(0,len(captcha)-1) # On génère un chiffre aléatoire dans la liste qui contient les lettres du mot choisi

        if y == 0:
            extension = "la première"
        elif y == len(captcha)-2:
            extension = "l'avant-dernière"
        elif y == len(captcha)-1:
            extension = "la dernière"
        else:
            extension = f"la {y+1}ème"

        reponse = str(input(f"Donner {extension} lettre du mot {str(mots[x]).upper()} : "))

        if reponse.lower() == str(captcha[y]) or reponse.upper() == str(captcha[y]): # On prend en charge les réponses en MAJUSCULE ou minuscule
            print("Ok !")
            verif_captcha = True # Si la réponse est ok on passe la valeur à True
        else:
            print("Réponse incorrecte !")
            tent += 1
            if tent == 5: # Après 5 tentatives on considère que le test a échoué
                print("Êtes-vous un robot ou un idiot !?")
                break

    return verif_captcha # On retourne True ou False en sortie de la fonction

# pycaptcha()